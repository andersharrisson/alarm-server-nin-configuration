# alarm-server-nin-configuration

Alarm server configuration
============

This repository is used to store all Alarm server configurations for neutron instruments.

The script *validate_xml* is used to validate all xml files against the Alarm Configuration Schema.
It is run automatically by the gitlab-ci pipeline.
You can run it locally on your machine (OSX and Linux) if you have xmllint installed.

When pushing to master, the new configuration is automatically deployed to the Alarm server on the TN.
